# Joyjet Mobile Developer Test #

Durante meu tempo livre implementei parcialmente a aplicação proposta em Swift, usando o máximo de componentes nativos do iOS.

### O que faltou no app: ###
-Implementar o design conforme o proposto, devido ao tempo implementei de inicio a tab bar. Cheguei a checar alguns componentes externos (Ex. [Link](https://github.com/dogo/AKSideMenu), porém não apliquei.

-Ficou faltando implementar a sessão Favoritos, iria implementar com Core Data, pois já trabalhei anteriormente com ele. Layout seria o mesmo da home, porém com uma consulta antes de carrega-la completamente.

-Ficou faltando fazer os requests das imagens, usei as de exemplo que já existem no componente. No caso, eu tinha que modificar o plist do projeto, pois as imagens eram em uma url http e por padrão só permitia https, só um pequeno detalhe corrigia isso.

-Tratar completamente o json, talvez fosse melhor utilizar MVC ou uma classe com os elementos do json, dicionário, etc. Já sabendo toda a estrutura do json o certo é deixar o preenchimento das imagens dos sliders, texto, tudo pelo json, acabei implementado somente nome das categorias e titulo dos itens.

Segue os links de alguns trabalhos anteriores em desenvolvimento mobile que fiz para consulta (os mesmos apps possuem uma versão em android, porém não fui o responsável):

[App Larbos](https://itunes.apple.com/br/app/larbos/id1152617425?mt=8)

[App Lab. Dr. Aurélio](https://itunes.apple.com/br/app/lab-dr-aurélio/id1165114147?mt=8)