//
//  HomeTableViewController.swift
//  Joylet Test
//
//  Created by Raimundo Farrapo Pinto Júnior on 08/03/17.
//  Copyright © 2017 Raimundo Farrapo Pinto Júnior. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeTableViewController: UITableViewController {
    
    var sections = [String]()
    var items = [[String]]()
    
    
    private func getValuesJson(){
        
        do{
            if let file = Bundle.main.url(forResource: "mobile-test-one", withExtension: "json"){
                let data = try Data(contentsOf: file)
                let json = JSON(data: data)

                //get names categories
                for (_, subJson) in json{
                    if let title = subJson["category"].string{
                        sections.append(title)
                    }
                }
                
                //get title item
                for category in 0..<sections.count{
                    var titleItem = [String]()
                    for (_, subJson) in json[category]["items"]{
                        if let title = subJson["title"].string{
                            titleItem.append(title)
                        }
                    }
                    items.append(titleItem)
                }
            } else {
                print("no file")
            }
        }catch{
            print(error.localizedDescription)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        getValuesJson()//funcao para nomear os item da tableview
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < sections.count {
            return sections[section]
        }
        
        return nil
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.items[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! HomeTableViewCell

        // Configure the cell...
        //Seta o nome da label de cada item da tableview
        cell.titleHomeCell.text = self.items[indexPath.section][indexPath.row]
        
        
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
