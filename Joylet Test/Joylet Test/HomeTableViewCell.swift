//
//  HomeTableViewCell.swift
//  Joylet Test
//
//  Created by Raimundo Farrapo Pinto Júnior on 08/03/17.
//  Copyright © 2017 Raimundo Farrapo Pinto Júnior. All rights reserved.
//

import UIKit
import ImageSlideshow

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var titleHomeCell: UILabel!
    
    let afNetworkingSource = [AFURLSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!,
                              AFURLSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!,
                              AFURLSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        slideShow.backgroundColor = UIColor.white
        slideShow.slideshowInterval = 2.0
        slideShow.pageControlPosition = PageControlPosition.underScrollView
        slideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        slideShow.pageControl.pageIndicatorTintColor = UIColor.black
        slideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideShow.setImageInputs(afNetworkingSource)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
